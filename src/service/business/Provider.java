package service.business;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import service.model.City;
import service.model.Country;

//Provides data to WorldService class
public class Provider {
	//for looking up country by code - key: code value: Country
	private Map<String, Country> countries = null;
	//for looking up country by name - key: name value: code
	private Map<String, String> countryCode = null;
	
	//for looking up city by key - key: ID value: City
	private Map<Integer, City> cities = null;
	
	public Provider() {
		loadData();
	}
	
	//Loads data into private fields from database
	private void loadData() {
		countries = new HashMap<>();
		countryCode = new HashMap<>();
		
		cities = new HashMap<>();
		
		Connection conn = DBConnection.getConnection();
		Statement stmt = null;
		ResultSet rs = null;
		
		try {
			stmt = conn.createStatement();
//For getting countries data
			String sql = "select Code, Code2, Name, Continent, Region, SurfaceArea, "
					+ " IndepYear, Population, LifeExpectancy, GNP, LocalName, "
					+ " Capital from world.country;";
			rs = stmt.executeQuery(sql);
			
			while(rs.next()){
				String Code = rs.getString("Code");
				String Code2 = rs.getString("Code2");
				String Name = rs.getString("Name");
				String Continent = rs.getString("Continent");
				String Region = rs.getString("Region");
				double SurfaceArea = rs.getDouble("SurfaceArea");
				int IndepYear = rs.getInt("IndepYear");
				long Population = rs.getInt("Population");
				double LifeExpentancy = rs.getDouble("LifeExpectancy");
				double GNP = rs.getDouble("GNP");
				String LocalName = rs.getString("LocalName");
				int Capital_city = rs.getInt("Capital");
				
				Country c = new Country(Code, Code2, Name, Continent, Region, 
						SurfaceArea, IndepYear, Population, LifeExpentancy, GNP, 
						LocalName, Capital_city);

				countries.put(Code.toLowerCase(), c);
				countryCode.put(Name.toLowerCase(), Code);
			} //End of getting countries data

// For getting Cities data
			sql = "select ID, Name, CountryCode, District, Population "
					+ " from world.city;";
			rs = stmt.executeQuery(sql);
			
			while(rs.next()){
				int ID = rs.getInt("ID");
				String Name = rs.getString("Name");
				String CountryCode = rs.getString("CountryCode");
				String District = rs.getString("District");
				long Population = rs.getInt("Population");
				
				City c = new City(ID, Name, CountryCode, District, Population);
				cities.put(ID, c);
			}
		} catch(SQLException se) {
		      se.printStackTrace();
		} catch(Exception e) {
		      e.printStackTrace();
		} finally {
			try {
				if(stmt!=null)
					stmt.close();
			} catch(SQLException se) {
		    }// do nothing
		      
			if(conn!=null)
				DBConnection.closeConnection();
			
		}//end try
	}
	
	//Returning list of countries
	public List<Country> getCountries() throws Exception {		
		if(countries.isEmpty()){
			throw new Exception("Oops! There may be an problem with the service.");
		}
		
		List<Country> list = new ArrayList<>(countries.values());
		
		return list;
	}

	//Returning Country that matches countries code with passed in parameter
	public Country getCountryByCode(String code) throws Exception {
		if(!countries.containsKey(code)){
			throw new Exception("Provided country code is invalid.");
		}
		return countries.get(code);
	}
	
	//Returning Country that matches countries name with passed in parameter
	public Country getCountryByName(String name) throws Exception {	
		String code = countryCode.get(name);

		if(!countries.containsKey(code.toLowerCase())){
			throw new Exception("Provided country name is invalid.");
		}
		
		return countries.get(code.toLowerCase());
	}

	//Returning list of cities
	public List<City> getCities() throws Exception {
		
		if(cities.isEmpty()){
			throw new Exception("Oops! There may be an problem with the service.");
		}
		
		List<City> list = new ArrayList<>(cities.values());
		
		return list;
	}

	//Returning city with ID equals to passed in parameter
	public City getCityByCode(int code) throws Exception {
		
		if(!cities.containsKey(code)){
			throw new Exception("Provided country name is invalid.");
		}
		return cities.get(code);
	}

	//Returning list of cities matching the name with passed in parameter
	public List<City> getCityByName(String name) throws Exception {
		
		if(cities.isEmpty()){
			throw new Exception("Oops! There may be an problem with the service.");
		}

		List<City> list = new ArrayList<>();
		for(City value : cities.values()) {
		    if(value.getName().toLowerCase().equals(name)){
		    	list.add(value);
		    }
		}
		
		if(list.isEmpty()){
			throw new Exception("Provided city name does not match any.");
		}
		return list;
	}
}
