package service.business;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class DBConnection {

	private static String driver = "com.mysql.jdbc.Driver";
	private static String url = "jdbc:mysql://localhost:3306/world"; 
	private static String userid = "user";
	private static String password = "password";
	private static Connection conn = null;
	
	public static Connection getConnection() {
	
		if(conn == null) {
			try {
				Class.forName(driver); 
				conn = DriverManager.getConnection(url, userid, password);
			} catch (SQLException exc) {
				System.out.println("connection failed with: " + exc.getMessage());
				return conn;
			} catch (ClassNotFoundException e) {
				System.out.println("FileNotFoundException: " + e.getMessage());
			}
		}

		return conn;
	}
	
    public static void closeConnection() {
    	if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				System.out.println( e.getMessage());
			}
    	}
    }

}
