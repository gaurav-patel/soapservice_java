package service.world;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebResult;
import javax.jws.WebService;

import service.business.Provider;
import service.model.City;
import service.model.Country;


@WebService(targetNamespace="http://www.worldinfo.net", serviceName="WorldService")
public class WorldService {
	
	private Provider service = new Provider();
	
	@WebMethod
	@WebResult(name="Country")
	public List<Country> getCountries() throws Exception {
		return service.getCountries();
	}
	
	@WebMethod
	@WebResult(name="Country")
	public Country getCountryByCode(String code) throws Exception {	
		return service.getCountryByCode(code.toLowerCase());
	}
	
	@WebMethod
	@WebResult(name="Country")
	public Country getCountryByName(String name) throws Exception {
		return service.getCountryByName(name.toLowerCase());
	}
	
	@WebMethod
	@WebResult(name="City")
	public List<City> getCities() throws Exception {
		return service.getCities();
	}
	
	@WebMethod
	@WebResult(name="City")
	public City getCityByCode(int code) throws Exception {
		return service.getCityByCode(code);
	}
	
	@WebMethod
	@WebResult(name="City")
	public List<City> getCityByName(String name) throws Exception {
		return service.getCityByName(name.toLowerCase());
	}
}
