package service.model;


public class Country {
	
	private String Code;
	private String Code2;
	private String Name;
	private String Continent;
	private String Region;
	private double SurfaceArea;
	private int IndepYear;
	private long Population;
	private double LifeExpentancy;
	private double GNP;
	private String LocalName;
	private int Capital_city;
	
	public Country(String code, String code2, String name, String continent, String region, double surfaceArea,
			int indepYear, long population, double lifeExpentancy, double gNP, String localName, int capital_city) {
	
		Code = code;
		Code2 = code2;
		Name = name;
		Continent = continent;
		Region = region;
		SurfaceArea = surfaceArea;
		IndepYear = indepYear;
		Population = population;
		LifeExpentancy = lifeExpentancy;
		GNP = gNP;
		LocalName = localName;
		Capital_city = capital_city;
	}

	public String getCode() {
		return Code;
	}

	public void setCode(String code) {
		Code = code;
	}

	public String getCode2() {
		return Code2;
	}

	public void setCode2(String code2) {
		Code2 = code2;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getContinent() {
		return Continent;
	}

	public void setContinent(String continent) {
		Continent = continent;
	}

	public String getRegion() {
		return Region;
	}

	public void setRegion(String region) {
		Region = region;
	}

	public double getSurfaceArea() {
		return SurfaceArea;
	}

	public void setSurfaceArea(double surfaceArea) {
		SurfaceArea = surfaceArea;
	}

	public int getIndepYear() {
		return IndepYear;
	}

	public void setIndepYear(int indepYear) {
		IndepYear = indepYear;
	}

	public long getPopulation() {
		return Population;
	}

	public void setPopulation(long population) {
		Population = population;
	}

	public double getLifeExpentancy() {
		return LifeExpentancy;
	}

	public void setLifeExpentancy(double lifeExpentancy) {
		LifeExpentancy = lifeExpentancy;
	}

	public double getGNP() {
		return GNP;
	}

	public void setGNP(double gNP) {
		GNP = gNP;
	}

	public String getLocalName() {
		return LocalName;
	}

	public void setLocalName(String localName) {
		LocalName = localName;
	}

	public int getCapital_city() {
		return Capital_city;
	}

	public void setCapital_city(int capital_city) {
		Capital_city = capital_city;
	}
	
}
